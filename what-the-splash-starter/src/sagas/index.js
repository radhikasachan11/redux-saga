import { takeEvery, put, take, call, all } from 'redux-saga/effects';
import { IMAGES } from '../constants';

import imagesSaga from './imagesSaga';
import statsSaga from './statsSaga';

export default function* rootSaga() {
    yield all([imagesSaga(), statsSaga()])
}

//export default imagesSaga;
//watcher saga - take, takeEvery
/* function* workerSaga() {
    console.log("Hey from worker");
    yield put({ type: 'ACTION_FROM_WORKER' });
} */

//watcher saga - take, takeEvery
/* function* byebyeSaga() {
    console.log("bye from worker");
    yield put({ type: 'ACTION_FROM_WORKER' });
} */

//section 5: setting redux
/* function* hanldeImagesLoad() {
    console.log("load images")
}

function* rootSaga() {

/*  // saga flow take, takeEvery 
    //yield takeEvery('Hello', workerSaga);
    yield take('Login');
    yield call(workerSaga);
    yield take('Logout');
    yield call(byebyeSaga); */

/* yield takeEvery(IMAGES.LOAD, hanldeImagesLoad);
    
}

export default rootSaga; */
