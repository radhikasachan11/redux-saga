import { takeEvery, select, call, put } from 'redux-saga/effects';
import { IMAGES } from '../constants';

import { fetchImages } from '../api';

import { setImages, setError } from '../actions';

export const getPage = state => state.nextPage;

//worker saga
export function* handleImagesLoad() {
    try {
        const page = yield select(getPage);
        const images = yield call(fetchImages, page);
        const test = 'ab';
        console.log('test', test);
        yield put(setImages(images));
    } catch (error) {
        yield put(setError(error.toString()));
    }
}

export default function* watchImagesLoad() {
    yield takeEvery(IMAGES.LOAD, handleImagesLoad);
}
