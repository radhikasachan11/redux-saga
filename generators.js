// 1. Creating Generator

//2.  For of: Iterating Generators
function* nameGenerator(){
    yield "Sam";
    yield "Tim";
    yield "Marley";
    yield "Done";
    
}
const nameResult = nameGenerator();

console.log("#For of iteration:")
for (result of nameResult){
    console.log(result);
}

console.log("");

//2.  .next() : yield and return values of generator
function* sayHiGenerator(){
    yield "Hello";
    yield "Welcome"
    yield "to";
    yield "Generators";
    return "hi";   
}

const hiResult = sayHiGenerator();

console.log("#Generator .next():");
console.log(hiResult.next());
console.log(hiResult.next());
console.log(hiResult.next());
console.log(hiResult.next());
console.log(hiResult.next());

console.log("");


//2.  .next("custom value") : custom value
function* customValueGenerator() {
    yield "custom"
    const result = yield "some value";
    return result;
}

const customValue = customValueGenerator();

console.log("#Generator .next('custom')");
console.log(customValue.next());
console.log(customValue.next());
console.log(customValue.next("Custom"));

console.log("");